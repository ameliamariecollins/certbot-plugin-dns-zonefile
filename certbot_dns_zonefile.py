"""DNS Zonefile: Modify BIND Zone Files
"""
import re
import shutil
import os
import os.path
import subprocess

from typing import Any
from typing import Callable
import zope.interface

from certbot import interfaces
from certbot.plugins import common
from certbot.plugins import dns_common


@zope.interface.implementer(interfaces.IAuthenticator)
@zope.interface.provider(interfaces.IPluginFactory)
class Authenticator(dns_common.DNSAuthenticator):
    """DNS Zonefile Authenticator."""

    description = "DNS Zonefile Authenticator"

    @classmethod
    def add_parser_arguments(cls, add):
        super(Authenticator, cls).add_parser_arguments(
            add, default_propagation_seconds=120
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.zone_file = os.environ['ZONE_FILE']
        self.zone = os.path.basename(self.zone_file)

    def _perform(self, domain, validation_name, validation):
        print("Challenge", validation_name)

        #-- Remove domain from validation name
        validation_name = validation_name[:-1-len(self.zone)]

        zone_file_old = '.'.join([self.zone_file, "old"])
        zone_file_new = '.'.join([self.zone_file, "new"])

        #-- Backup zone file
        shutil.copy(self.zone_file, zone_file_old)

        #-- Read entire file
        with open(self.zone_file) as f:
            lines = f.readlines()

        found_serial = False
        found_validation = False

        with open(zone_file_new, "w") as f:
            for line in lines:
                # Increment serial number
                m = re.match(r'(.*)([0-9]{10})(.*)$', line)
                if m:
                    found_serial = True
                    serial = int(m[2])
                    f.write("%s%s%s\n" % (m[1], serial+1, m[3]))
                    continue
                # Replace Validation
                m = re.search('(%s)(\s.*)(".*")' % validation_name, line)
                if m:
                    found_validation = True
                    f.write('%s%s"%s"\n' % (m[1], m[2], validation))
                    continue
                # Other lines are verbatim
                f.write(line.rstrip())

        if not found_serial:
            raise RuntimeError("Did not find serial number")
        if not found_validation:
            raise RuntimeError("Did not find validation")

        shutil.move(zone_file_new, self.zone_file)

        subprocess.run(['systemctl', 'reload', 'bind9'], check=True)

    def more_info(self): pass
    def _get_chall_pref(self): pass
    def _prepare(self): pass
    def _setup_credentials(self): pass
    def _cleanup(self, domain: str, validation_name: str,
            validation: str) -> None: pass

