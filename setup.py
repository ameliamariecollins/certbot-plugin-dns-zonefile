from setuptools import setup

setup(
    name = 'certbot-dns-zonefile',
    package = 'certbot_dns_zonefile.py',
    install_requires = ['certbot'],
    entry_points = {
        'certbot.plugins': [
            'dns-zonefile = certbot_dns_zonefile:Authenticator'
        ]
    }
)
